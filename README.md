This is a testing tool for comparing two REST API versions. It highlights differences between the APIs responses, using fixed samples of DOIs, funders, members, journals and queries.

To compare the metadata:

```
NEW_URL=<new API URL> OLD_URL=<old API URL> lein run works_metadata
NEW_URL=<new API URL> OLD_URL=<old API URL> lein run members_metadata
NEW_URL=<new API URL> OLD_URL=<old API URL> lein run journals_metadata
NEW_URL=<new API URL> OLD_URL=<old API URL> lein run funders_metadata
```

To compare DOIs returned from queries:

```
NEW_URL=<new API URL> OLD_URL=<old API URL> lein run queries
```

